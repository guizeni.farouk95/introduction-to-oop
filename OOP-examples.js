function MakeAccount(initial) {
    var account = {};
    account.balance = initial;
    account.withdraw = withdraw;
    account.deposit = deposit;
    return account;
}

var withdraw = function(amount) {
    if (this.balance - amount >= 0) {
        this.balance = this.balance - amount;
        return 'Here is your money: $' + amount;
    }
    return 'Insufficient funds.';
};
var deposit = function(amount) {
    this.balance = this.balance + amount;
    return 'Your balance is: $' + this.balance;
};
////////////// Second method ////////////////
class MakeAccount{
    constructor(initial){
        this.balance = initial;
    }
    withdraw (amount) {
        if (this.balance - amount >= 0) {
            this.balance = this.balance - amount;
            return 'Here is your money: $' + amount;
        }
        return 'Insufficient funds.';
    }
    deposit(amount){
        this.balance = this.balance + amount;
        return 'Your balance is: $' + this.balance;
    }
}